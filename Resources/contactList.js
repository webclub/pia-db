var win = Ti.UI.currentWindow;

// Knapp för att skapa ny kontakt
var addContactButton = Ti.UI.createButton({
	title: "Ny",
	width: "auto",
	height: "auto"
}); 

addContactButton.addEventListener("click", function(e) {
	// Skapa fönster för att skapa ny kontakt och öppna via navigeringen
	var contactWindow = Titanium.UI.createWindow({
		url: "contactView.js", 
		title: 'Ny kontakt'
	});
	
	Ti.App.mynav.open(contactWindow, {animated:true});
});
// Sätt vår skapa kontakt knapp till navigeringsytan
win.rightNavButton = addContactButton;

// Array/lista för att lagra våra rader i tableview
var tableData = [];

// Table view
var contactTableView = Ti.UI.createTableView({
	editable: true
});

// Event för om man raderar något i table view
contactTableView.addEventListener("delete", function(e) {
	// Öppna databasen
	var db = Ti.Database.open("kontakter");
	
	// Radera i tabellen "contacs" där rowid dvs idnummer är id som sattes till row längre ner i koden
	db.execute('DELETE FROM contacts WHERE rowid = '+e.rowData.dbRowId);
	
	db.close();

});

contactTableView.addEventListener("click", function(e) {
	// Vid tryck på rad öppnas fönster och vi skickar med idnummer till rad i databasen
	var contactWindow = Titanium.UI.createWindow({
	    url: "contactView.js", 
	    title: 'Redigera kontakt',
	    contactId: e.rowData.dbRowId
	});
	
	Ti.App.mynav.open(contactWindow, {animated:true});
});

win.add(contactTableView);

// Funktion för att uppdatera table view
Ti.App.addEventListener("updateContactList", function(e) {
	// Börja med att tömma listan
	tableData = [];
	
	var db = Ti.Database.open("kontakter");
	
	// Hämta från databasen (select) kolumnerna rowid,name och phone_number från tabell contacts 
	// Sortera efter namn i bokstavsordning stigande a-ö
	var rows = db.execute('SELECT rowid,name,phone_number FROM contacts ORDER BY name ASC');
	
	// Loopa igenom alla resultat
	while (rows.isValidRow())
	{
		Ti.API.info('Person ---> ROWID: ' + rows.fieldByName('rowid') + ', name:' + rows.field(1) + ', phone_number: ' + rows.fieldByName('phone_number'));
		// Skapa rad i table view
		var row = Ti.UI.createTableViewRow({
			height: 50,
			dbRowId: rows.fieldByName('rowid')
		});
		// Lägg till label med namn och telefonnummer
		var rowLabel = Ti.UI.createLabel({
			text: rows.fieldByName('name')+" "+rows.fieldByName('phone_number'),
			left: 10
		});
		
		row.add(rowLabel);
		// Lägg till raden i tableData som lagrar raderna
		tableData.push(row);
		
		rows.next();
	}
	rows.close();
	
	db.close();
	// Sätt tableData som raderna i tableview
	contactTableView.setData(tableData);
	
});
// Kör funktionen så det visas rader från början
Ti.App.fireEvent("updateContactList");

